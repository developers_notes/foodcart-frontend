import { NgModule } from '@angular/core';
import { CustomerComponent } from '../pages/customer/customer.component';
import { KitchenComponent } from '../pages/kitchen/kitchen.component';
import { CashierComponent } from '../pages/cashier/cashier.component';

@NgModule({
  imports: [],
  exports: [],
  declarations: [
      CustomerComponent,
      KitchenComponent,
      CashierComponent,
  ],
})
export class PagesModule {
}
