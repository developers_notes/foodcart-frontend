import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'login',
    redirectTo: '/users/login', // '/users/login',
    canActivate: [],
    pathMatch: 'full',
  },
  {
    path: 'customer',
    loadChildren: 'src/app/pages/customer/customer.module#CustomerModule',
    canActivate: [/*AuthenticationGuard*/]
  },
  {
    path: 'kitchen',
    loadChildren: 'src/app/pages/kitchen/kitchen.module#KitchenModule',
    canActivate: [/*AuthenticationGuard*/]
  },
  {
    path: 'cashier',
    loadChildren: 'src/app/pages/cashier/cashier.module#CashierModule',
    canActivate: [/*AuthenticationGuard*/]
  },
  { path: '**', redirectTo: '/', pathMatch: 'full' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true, onSameUrlNavigation: 'reload' })],
  providers: [],
  exports: [RouterModule]
})
export class AppRoutingModule { }
