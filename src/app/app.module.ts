import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { AppComponent } from './app.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { PagesModule } from './pages/pages.module';
import { MaterialModule } from './core/material-module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// const config: SocketIoConfig = { url: 'ws://localhost:4000', options: {} };

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    //SocketIoModule.forRoot(config),
    AppRoutingModule,
    MaterialModule,
    ReactiveFormsModule, FormsModule,
    PagesModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
