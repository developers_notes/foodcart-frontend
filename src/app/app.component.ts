import { Component, OnInit } from '@angular/core';
import { OrderSocketService } from './shared/services/order-socket.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'food-cart8';
  newMessage: string;
  messageList:  string[] = [];

  constructor(private readonly orderSocketService: OrderSocketService) {}

  ngOnInit() {
    this.orderSocketService.listen('message').subscribe( (ret: any)  => {
      console.log('receive from server: ', ret);
      this.messageList.push(ret);
    })
  }

  sendMessage() {
    this.orderSocketService.emit('message', this.newMessage);
    this.newMessage = '';
  }

}
