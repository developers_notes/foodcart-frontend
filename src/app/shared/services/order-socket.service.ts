import { Injectable } from '@angular/core';
import * as io from "socket.io-client/dist/socket.io";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrderSocketService {

  socket: any;
  private readonly uri: string = "ws://localhost:4000";
  constructor(){
    this.socket = io(this.uri);
  }

  listen(eventname: string) {
    return new Observable((subscriber) => {
      this.socket.on(eventname, (data) => {
        subscriber.next(data);
      })
    })
  }

  emit(eventname, data) {
    this.socket.emit(eventname, data);
  }

}
